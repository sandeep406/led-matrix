/*
   FILE          : Final project.ino
   PROJECT       : PROG8125 - Project
   PROGRAMMER    : Sandeep (Student)
   FIRST VERSION : 2016-04-16
   DESCRIPTION   : This code is used to display initials of mine in different patterns.
                   Even i used an interrupt to observe an individual pattern specifically that helps us to take a look at one different pattern
*/

#include<Bounce.h>            
//  ******************* Prototype delerations************************** 
  void refreshDisplay(void);   

//Globals

//                        r0,r1,r2,r3,r4,r5,r6,r7
  const int rowPins[8] = { 2, 3, 4, 5, 6 ,7, 8, 9};             //  array of row pin numbers:
//                        c0,c1,c2,c3,c4,c5,c6,c7
  const int colPins[8] = {23,22,21,20,19,18,17,16};            //  array of column pin numbers:
// 2-dimensional array of ledMatrix:
  int ledMatrix[8][8];                                          // 2-dimensional array indicates rows and columns
  unsigned long  displayRate = 0;                              //unsigned long is because if we want to decrease the speed of each pattern 
//                                                             we can do it by increasingto control display speed
  uint8_t  selectPattern = 0;                                  // use to change patterns,by using uint it wont take negetive values

//Created my own datatype that can hold all my pattern array which i want to display
//By doing like this,if we want to add an another pattern we can directly add it with ease

typedef bool ledPatternDisplay[8][8];                       //Arrays with bool variables which are used to display in a specific fornat

 const ledPatternDisplay pattern3 =
 {
 {0, 0, 1, 1, 1, 1, 0, 0},
 {0, 1, 0, 0, 0, 0, 1, 0},
 {1, 0, 1, 0, 0, 1, 0, 1},
 {1, 0, 0, 0, 0, 0, 0, 1},                              //This displays pattern3 symbol as a welcome screen
 {1, 0, 1, 0, 0, 1, 0, 1},
 {1, 0, 0, 1, 1, 0, 0, 1},
 {0, 1, 0, 0, 0, 0, 1, 0},
 {0, 0, 1, 1, 1, 1, 0, 0}
 };
 const ledPatternDisplay pattern1 = 
 {
  {1, 1, 1, 1, 0, 0, 0, 0},
  {1, 1, 1, 1, 0, 0, 0, 0},
  {1, 1, 1, 1, 0, 0, 0, 0},                           
  {1, 1, 1, 1, 0, 0, 0, 0},                        //This is to demonstrate that pattern of iniials are going to get stert
  {1, 1, 1, 1, 0, 0, 0, 0},
  {1, 1, 1, 1, 0 ,0, 0, 0},
  {1, 1, 1, 1, 0 ,0, 0, 0},
  {1, 1, 1, 1, 0, 0, 0, 0}
  };

 const ledPatternDisplay pattern4 =
 {
  {1, 1, 1, 1,  0, 1, 1, 0},
  {1, 0, 0, 0,  1, 0, 0, 1},
  {1, 0, 0, 0,  1, 0, 0, 1},
  {1, 1, 1, 1,  1, 1, 1, 1},                     //This is my first pattern which displays big S and A patern in a specific way
  {0, 0, 0, 1,  1, 0, 0, 1},
  {0, 0, 0, 1,  1, 0, 0, 1},
  {1, 1, 1, 1,  1, 0, 0, 1},
  {0, 0, 0, 0,  0, 0, 0, 0}
 };

 const ledPatternDisplay pattern2 = 
 {
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},                               //This is my second pattern which displays shrinking S and A patern in a specific way
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 1, 1, 1}
 };

 const ledPatternDisplay pattern5 =
 {
  {1, 1, 1, 1,  1, 1, 1, 1},
  {1, 0, 0, 0,  0, 0, 0, 1},
  {1, 0, 1, 1,  1, 1, 0, 1},
  {1, 0, 1, 0,  0, 1, 0, 1},                        //This is my third pattern which displays narrow S and A patern in a specific way
  {1, 0, 1, 0,  0, 1, 0, 1},
  {1, 0, 1, 1,  1, 1, 0, 1},
  {1, 0, 0, 0,  0, 0, 0, 1},
  {1, 1, 1, 1,  1, 1, 1, 1}
 };

 const ledPatternDisplay pattern6 = 
 {
  {1, 1, 1, 0,  0, 1, 0, 0},
  {1, 0, 0, 0,  1, 0, 1, 0},
  {1, 0, 0, 0,  1, 0, 1, 0},
  {1, 1, 1, 0,  1, 1, 1, 0},                     //This is my fourth pattern which displays left shift of  big S and A patern  in a specific way
  {0, 0, 1, 0,  1, 0, 1, 0},
  {0, 0, 1, 0,  1, 0, 1, 0},
  {1, 1, 1, 0,  1, 0, 1, 0},
  {0, 0, 0, 0,  0, 0, 0, 0}
 };

 const ledPatternDisplay pattern7 = 
 {
  {1, 1, 1, 0,  0, 0, 0, 0},
  {1, 0, 0, 0,  0, 0, 0, 0},
  {1, 1, 1, 0,  0, 0, 0, 0},
  {0, 0, 1, 0,  0, 1, 1, 0},                        //This is my fifth pattern which displays small S on top left corner and A on bottom right corner
  {1, 1, 1, 0,  1, 0, 0, 1},
  {0, 0, 0, 0,  1, 1, 1, 1},
  {0, 0, 0, 0,  1, 0, 0, 1},
  {0, 0, 0, 0,  0, 0, 0, 0}
 };
  const ledPatternDisplay pattern8 = 
  {
  {1, 0, 1, 0,  1, 0, 0, 0},
  {1, 0, 1, 0,  1, 0, 0, 0},
  {1, 0, 1, 0,  1, 0, 0, 0},
  {1, 0, 1, 0,  1, 1, 1, 1},                  //This is my sixth pattern which displays small S on bottom left position and small A patern in top right
  {1, 0, 1, 0,  0, 0, 0, 0},
  {1, 0, 1, 1,  1, 1, 1, 1},
  {1, 0, 0, 0,  0, 0, 0, 0},
  {1, 1, 1, 1,  1, 1, 1, 1}
  };

// pattern pointer is an array which stores all the addresses of all patterns declared in pattern datatype
   
   const ledPatternDisplay *patternPointer[8] = {&pattern1, &pattern2, &pattern3, &pattern4, &pattern5, &pattern6, &pattern7, &pattern8};

//*************************************************************************************************************************************************************
// FUNCTION      : crefreshDisplay()
//DESCRIPTION   : This function is used to refresh the disply screen
//               By ensuring all the leds are on ,Because this is common cathode led display
//PARAMETERS    : This function does not accept any parameters
//RETURNS       : returns nothing
//***************************************************************************************************************************************************************

 void refreshDisplay(void)
{
 for (int i = 0; i < 8; i++)                            // iterate over the rows (anodes):
  {
    digitalWrite(rowPins[i], HIGH);                    // take the row pin (anode) high:
    for (int j = 0; j < 8; j++)                       // iterate over the cols (cathodes):
    {                                                // get the state of the current pixel;
     int thisPixel = ledMatrix[i][j];                  // when the row is HIGH and the col is LOW,
     digitalWrite(colPins[j], thisPixel);           // the LED where they meet turns on:
      if (thisPixel == LOW)                          // turn the pixel off:
      {
        digitalWrite(colPins[j], HIGH);
      }
    }
      digitalWrite(rowPins[i], LOW);               // take the row pin low to turn off the whole row:
  }
}
//*************************************************************************************************************************************************************


//*************************************************************************************************************************************************************
// FUNCTION      :  displayInitials()
//DESCRIPTION    : This function is used to display initials in different patterns by maping 
//                 pattern pointer array values to another pointer and used to display it on display
//PARAMETERS    :  This function does not accept any parameters
//RETURNS       :  returns nothing

void displayInitials(void)
{
 const ledPatternDisplay *patternMap = patternPointer[selectPattern];
  for (int8_t x = 0; x < 8; x++) 
  {
    for (int8_t y = 0; y < 8; y++) 
    {
     bool value = (*patternMap)[x][y];                    //reads 'value' at corresponding address since only 0 and 1 bool is used
      {
      if(value ==1)                                      // when particular pixel is high acording to led matrix
      {
        ledMatrix[x][y] = LOW;                           // it will glow that partiular led
      }
      else
      {
        ledMatrix[x][y] = HIGH;                         // it will off that partiular led 
      }
    }
    }
  }
  selectPattern++;                                    // it eill be incremented to next pattern
  if(selectPattern>7)
  {
    selectPattern = 0;                               // so selectPattern will be resetted
  }
}

void setup()
{
  Serial.begin(9600);
  pinMode(12, INPUT_PULLUP);
  // iterate over the pins: 
  for (int ledPin = 0; ledPin < 8; ledPin++)
  {
    pinMode(colPins[ledPin], OUTPUT);           // initialize the output pins:
    pinMode(rowPins[ledPin], OUTPUT);
   digitalWrite(colPins[ledPin], HIGH);          //take the col pins (i.e. the cathodes) high to ensure that the LEDS are off:  
  } 
}


void loop()
{
// refreshes the screen
  refreshDisplay();                    //refreshes display
  displayRate++;                       //increments display rate till 10000 
   if(displayRate == 50000  )            // for every 10000 counts
    {
   displayInitials();                 // displayInitials function is called and displays initials
    displayRate = 0;                  // reset displayRate to zero
    }
}






